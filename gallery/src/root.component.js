import React from "react";
import axios from 'axios';

export default class Gallery extends React.Component {
  state = {
    images: []
  };

  componentDidMount() {
    axios.get('https://api.giphy.com/v1/gifs/search?q=cat&api_key=dc6zaTOxFJmzC').then(res => {
      this.setState({ images: res.data.data });
    }).catch(err => {
      console.log(err);
    });
  }

  render() {
    let images = this.state.images.map(image => {
      return (
        <div key={image.id}>
          <img src={image.images.downsized_medium.url} alt="" />
        </div>
      );
    });

    return (
      <div>
        <h2>Cats Section</h2>
        {images}
      </div>
    );
  }
}

