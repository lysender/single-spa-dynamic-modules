# POC for Single SPA Dynamic Modules


## Bootstrapping each mini-apps

### Install required packages

```text
# Core package
yarn add single-spa

# Babel packages
yarn add --dev @babel/core @babel/preset-env @babel/preset-react @babel/plugin-syntax-dynamic-import @babel/plugin-proposal-object-rest-spread @babel/plugin-proposal-class-properties

# Webpack core
yarn add webpack webpack-dev-server webpack-cli --dev

# Webpack plugins
yarn add clean-webpack-plugin copy-webpack-plugin --dev

# Webpack loaders
yarn add style-loader css-loader html-loader babel-loader --dev

# Add React packages
yarn add react react-dom single-spa-react react-router-dom react-transition-group

# Styling package
yarn add kremling
yarn add kremling-loader --dev

# Additional packages
yarn add @reach/router
yarn add autoprefixer --dev

```

### Setup Babel at `.babelrc` under the mini-app directory

```text
{
  "presets": [
    ["@babel/preset-env", {
      "targets": {
        "browsers": ["last 2 versions"]
      }
    }],
    ["@babel/preset-react"]
  ],
  "plugins": [
    "@babel/plugin-syntax-dynamic-import",
    "@babel/plugin-proposal-object-rest-spread",
    "@babel/plugin-proposal-class-properties"
  ]
}
```

### Setup dev web server at `webpack.dev.js`

```javascript
const config = require('./webpack.config.js');
const webpack = require('webpack');

config.plugins.push(new webpack.NamedModulesPlugin());
config.plugins.push(new webpack.HotModuleReplacementPlugin());
config.devServer = {
  headers: {
    "Access-Control-Allow-Origin": "*",
  },
};

config.mode = 'development';

module.exports = config;
```

### Setup webpack config at `webpack.config.js` (example only)

```javascript
const webpack = require('webpack')
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, 'src/navbar.app.js'),
  output: {
    filename: 'navbar.app.js',
    library: 'navbar',
    libraryTarget: 'amd',
    path: path.resolve(__dirname, 'build/navbar'),
  },
  mode: 'production',
  module: {
    rules: [
      {parser: {System: false}},
      {
        test: /\.js?$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        exclude: [path.resolve(__dirname, 'node_modules'), /\.krem.css$/],
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[path][name]__[local]',
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins() {
                return [
                  require('autoprefixer')
                ];
              },
            },
          },
        ],
      },
      {
        test: /\.css$/,
        include: [path.resolve(__dirname, 'node_modules')],
        exclude: [/\.krem.css$/],
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.krem.css$/,
        exclude: [path.resolve(__dirname, 'node_modules')],
        use: [
          {
            loader: 'kremling-loader',
            options: {
              namespace: 'app-dashboard-ui',
              postcss: {
                plugins: {
                  'autoprefixer': {}
                }
              }
            },
          },
        ]
      },
    ],
  },
  resolve: {
    modules: [
      __dirname,
      'node_modules',
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {from: path.resolve(__dirname, 'src/navbar.app.js')}
    ]),
  ],
  devtool: 'source-map',
  externals: [
    /^lodash$/,
    /^single-spa$/,
    /^react$/,
    /^react\/lib.*/,
    /^react-dom$/,
    /.*react-dom.*/,
    /^rxjs\/?.*$/,
  ],
};
```

### Add `scripts` section into `package.json` (adjust ports)

```json
{
  "scripts": {
    "start": "webpack-dev-server --config ./webpack.dev.js --port 8401"
  }
}
```

### Packages for combining them all at top level

```bash
yarn add @babel/core @babel/plugin-syntax-dynamic-import @babel/preset-env babel-core babel-loader clean-webpack-plugin concurrently copy-webpack-plugin css-loader style-loader webpack webpack-cli webpack-dev-server --dev
```

### Add `scripts` section at top level `package.json`

```json
{
  "scripts": {
    "start": "concurrently --kill-others --kill-others-on-fail -p name --names \"config,common-deps,navbar,people,planets,fetchWithCache\" \"npm run start:config\" \"npm run start:common-deps\" \"npm run start:navbar\" \"npm run start:people\" \"npm run start:planets\" \"npm run start:fetchWithCache\"",
  }
}
```
