import React from 'react';
import burgerLogo from '../../assets/images/burger-logo.png';

export const Logo = () => {
  let baseUrl = "";
  if (process.env.MICRO_BASE_URL) {
    baseUrl = process.env.MICRO_BASE_URL;
  }
  return (
    <div className="Logo">
      <img src={baseUrl + burgerLogo} alt="Burger Logo" />
    </div>
  );
};
