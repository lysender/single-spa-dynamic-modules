export const links = [
  {
    name: 'Colors',
    href: '/colors'
  },
  {
    name: 'Gallery',
    href: '/gallery'
  }
];
