import * as isActive from './activityFns.js'
import * as singleSpa from 'single-spa'

singleSpa.registerApplication('navbar', () => SystemJS.import('@portal/navbar'), isActive.navbar);
singleSpa.registerApplication('colors', () => SystemJS.import('@portal/colors'), isActive.colors);
singleSpa.registerApplication('gallery', () => SystemJS.import('@portal/gallery'), isActive.gallery);

singleSpa.start();
