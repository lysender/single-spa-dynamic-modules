export function prefix(location, ...prefixes) {
  return prefixes.some(
    prefix => (
      location.href.indexOf(`${location.origin}/${prefix}`) !== -1
    )
  )
}

export function navbar(location) {
  return true
}

export function gallery(location) {
  return prefix(location, 'gallery')
}

export function colors(location) {
  return prefix(location, 'colors')
}
